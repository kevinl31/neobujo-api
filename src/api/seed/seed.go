package seed

import (
	"log"
	"time"

	"api/models"

	"github.com/jinzhu/gorm"
)

var users = []models.User{
	models.User{
		ID:        1,
		Firstname: "Kevin",
		Lastname:  "Let",
		Username:  "kevkev",
		Email:     "kevin@gmail.com",
		Password:  "password",
	},
	models.User{
		ID:        2,
		Firstname: "Nicolas",
		Lastname:  "Let",
		Username:  "Dieu",
		Email:     "nicolas@gmail.com",
		Password:  "password",
	},
}

var dashboards = []models.Dashboard{
	models.Dashboard{
		ID:     1,
		Type:   "weekly",
		Name:   "My weekly dash",
		UserID: 1,
	},
	models.Dashboard{
		ID:     2,
		Type:   "monthly",
		Name:   "My monthly dash",
		UserID: 2,
	},
}

var items = []models.Item{
	models.Item{
		ID:          1,
		Type:        "note",
		Name:        "My first note",
		Date:        time.Date(2020, 4, 2, 0, 0, 0, 0, time.Local),
		DashboardID: 1,
	},
	models.Item{
		ID:          2,
		Type:        "task",
		Name:        "My first task",
		Date:        time.Date(2020, 4, 11, 0, 0, 0, 0, time.Local),
		DashboardID: 2,
	},
	models.Item{
		ID:          3,
		Type:        "event",
		Name:        "My first event",
		Date:        time.Date(2020, 4, 10, 0, 0, 0, 0, time.Local),
		DashboardID: 1,
	},
	models.Item{
		ID:          4,
		Type:        "note",
		Name:        "My second task",
		Date:        time.Date(2020, 4, 16, 0, 0, 0, 0, time.Local),
		DashboardID: 1,
	},
}

func Load(db *gorm.DB) {

	err := db.Debug().DropTableIfExists(&models.Item{}, &models.Dashboard{}, &models.User{}).Error
	if err != nil {
		log.Fatalf("cannot drop table: %v", err)
	}
	err = db.Debug().AutoMigrate(&models.Item{}, &models.Dashboard{}, &models.User{}).Error
	if err != nil {
		log.Fatalf("cannot migrate table: %v", err)
	}

	err = db.Debug().Model(&models.Item{}).AddForeignKey("dashboard_id", "dashboards(id)", "CASCADE", "CASCADE").Error
	if err != nil {
		log.Fatalf("attaching foreign key error: %v", err)
	}
	err = db.Debug().Model(&models.Dashboard{}).AddForeignKey("user_id", "users(id)", "CASCADE", "CASCADE").Error
	if err != nil {
		log.Fatalf("attaching foreign key error: %v", err)
	}

	for i := range users {
		err = db.Debug().Model(&models.User{}).Create(&users[i]).Error
		if err != nil {
			log.Fatalf("cannot seed users table: %v", err)
		}
	}

	for i := range dashboards {
		err = db.Debug().Model(&models.Dashboard{}).Create(&dashboards[i]).Error
		if err != nil {
			log.Fatalf("cannot seed dashboard table: %v", err)
		}
	}

	for i := range items {
		err = db.Debug().Model(&models.Item{}).Create(&items[i]).Error
		if err != nil {
			log.Fatalf("cannot seed items table: %v", err)
		}
	}
}
