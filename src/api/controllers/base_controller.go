package controllers

import (
	"api/models"
	"fmt"
	"log"
	"net/http"

	"github.com/gorilla/mux"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
)

type Server struct {
	DB     *gorm.DB
	Router *mux.Router
}

func (server *Server) Initialize(dbDriver, dbHost, dbPassword, dbUser, dbName, dbPort string) {
	var err error

	if dbDriver == "mysql" {
		dbURL := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?charset=utf8&parseTime=True&loc=Local", dbUser, dbPassword, dbHost, dbPort, dbName)
		server.DB, err = gorm.Open(dbDriver, dbURL)
		if err != nil {
			fmt.Printf("Cannot connect to %s database with host %s and user %s.", dbDriver, dbHost, dbUser)
			log.Fatal("This is the error:", err)
		} else {
			fmt.Printf("We are connected to the %s database with host %s and user %s.", dbDriver, dbHost, dbUser)
		}
	}

	server.DB.Debug().AutoMigrate(models.User{}, models.Dashboard{}, models.Item{})
	server.DB.Debug().Model(&models.Item{}).AddForeignKey("dashboard_id", "dashboards(id)", "CASCADE", "CASCADE")
	server.DB.Debug().Model(&models.Dashboard{}).AddForeignKey("user_id", "users(id)", "CASCADE", "CASCADE")

	server.Router = mux.NewRouter()
	server.initializeRoutes()
}

func (server *Server) Run(addr string) {
	fmt.Println("Listening to port 9000")
	log.Fatal(http.ListenAndServe(addr, server.Router))
}
