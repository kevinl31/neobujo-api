package controllers

import "api/middlewares"

func (s *Server) initializeRoutes() {

	s.Router.HandleFunc("/", middlewares.SetJSONMiddleware(s.Home)).Methods("GET")

	s.Router.HandleFunc("/login", middlewares.SetJSONMiddleware(s.Login)).Methods("POST")

	// Users Routes
	s.Router.HandleFunc("/user", middlewares.SetJSONMiddleware(s.CreateUser)).Methods("POST")
	s.Router.HandleFunc("/users", middlewares.SetJSONMiddleware(s.GetUsers)).Methods("GET")
	s.Router.HandleFunc("/user/{id}", middlewares.SetJSONMiddleware(s.GetUser)).Methods("GET")
	s.Router.HandleFunc("/user/{id}", middlewares.SetJSONMiddleware(middlewares.SetAuthMiddleware(s.UpdateUser))).Methods("PUT")
	s.Router.HandleFunc("/user/{id}", middlewares.SetAuthMiddleware(s.DeleteUser)).Methods("DELETE")

	// Dashboards Routes
	s.Router.HandleFunc("/dashboard", middlewares.SetJSONMiddleware(s.CreateDashboard)).Methods("POST")
	s.Router.HandleFunc("/dashboards", middlewares.SetJSONMiddleware(s.GetDashboards)).Methods("GET")
	s.Router.HandleFunc("/dashboard/{id}", middlewares.SetJSONMiddleware(s.GetDashboard)).Methods("GET")
	s.Router.HandleFunc("/dashboard/{id}", middlewares.SetJSONMiddleware(middlewares.SetAuthMiddleware(s.UpdateDashboard))).Methods("PUT")
	s.Router.HandleFunc("/dashboard/{id}", middlewares.SetAuthMiddleware(s.DeleteDashboard)).Methods("DELETE")

	//Items Routes
	s.Router.HandleFunc("/item", middlewares.SetJSONMiddleware(s.CreateItem)).Methods("POST")
	s.Router.HandleFunc("/items", middlewares.SetJSONMiddleware(s.GetItems)).Methods("GET")
	s.Router.HandleFunc("/item/{id}", middlewares.SetJSONMiddleware(s.GetItem)).Methods("GET")
	s.Router.HandleFunc("/item/{id}", middlewares.SetJSONMiddleware(middlewares.SetAuthMiddleware(s.UpdateItem))).Methods("PUT")
	s.Router.HandleFunc("/item/{id}", middlewares.SetAuthMiddleware(s.DeleteItem)).Methods("DELETE")
}
