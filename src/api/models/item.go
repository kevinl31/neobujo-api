package models

import (
	"errors"
	"html"
	"strings"
	"time"

	"github.com/jinzhu/gorm"
)

// Item is base entity of bujork representing a note,
// a todo, an event, etc ...
type Item struct {
	ID          int       `gorm:"primary_key" json:"id"`
	Name        string    `gorm:"size:512" json:"name"`
	Type        string    `gorm:"type:varchar(10)" json:"type"`
	CreatedAt   time.Time `gorm:"default:CURRENT_TIMESTAMP" json:"created_at"`
	UpdatedAt   time.Time `gorm:"default:CURRENT_TIMESTAMP" json:"updated_at"`
	Date        time.Time `json:"date"`
	IsCompleted bool      `json:"is_completed"`
	IsPro       bool      `json:"is_pro"`
	DashboardID int       `json:"user_id"`
	Dashboard   Dashboard `json:"dashboard"`
}

func isInvalidItemType(itemType string) bool {
	switch itemType {
	case
		"task",
		"event",
		"note":
		return false
	}
	return true
}

func (i *Item) Prepare() {
	i.Name = html.EscapeString(strings.TrimSpace(i.Name))
	i.CreatedAt = time.Now()
	i.UpdatedAt = time.Now()
}

func (i *Item) Validate() error {
	if i.Name == "" {
		return errors.New("Required Name")
	}
	if isInvalidItemType(i.Type) {
		return errors.New("Invalid item type")
	}
	if i.Date.IsZero() {
		return errors.New("Date for item must be set")
	}
	if i.DashboardID < 1 {
		return errors.New("Invalid Dashboard Id")
	}
	return nil
}

func (i *Item) SaveItem(db *gorm.DB) (*Item, error) {
	err := db.Debug().Model(&Item{}).Create(&i).Error
	if err != nil {
		return &Item{}, err
	}
	if i.ID != 0 {
		// Fetching Dashboard related to the item
		err = db.Debug().Model(&Dashboard{}).Where("id = ?", i.DashboardID).Take(&i.Dashboard).Error
		if err != nil {
			return &Item{}, err
		}
	}
	return i, nil
}

func (i *Item) FindAllItems(db *gorm.DB) (*[]Item, error) {
	items := []Item{}
	err := db.Debug().Model(&Item{}).Limit(100).Find(&items).Error
	if err != nil {
		return &[]Item{}, err
	}
	// Wondering if it's the best way to fetch User ...
	if len(items) > 0 {
		for i, _ := range items {
			err := db.Debug().Model(&Dashboard{}).Where("id = ?", items[i].DashboardID).Take(&items[i].Dashboard).Error
			if err != nil {
				return &[]Item{}, err
			}
		}
	}
	return &items, nil
}

func (i *Item) FindItemByID(db *gorm.DB, did int) (*Item, error) {
	err := db.Debug().Model(&Item{}).Where("id = ?", did).Take(&i).Error
	if err != nil {
		return &Item{}, err
	}
	if i.ID != 0 {
		err := db.Debug().Model(&Dashboard{}).Where("id = ?", i.DashboardID).Take(&i.Dashboard).Error
		if err != nil {
			return &Item{}, err
		}
	}
	return i, nil
}

func (i *Item) UpdateItem(db *gorm.DB) (*Item, error) {
	err := db.Debug().Model(&Item{}).Where("id = ?", i.ID).Updates(Item{Name: i.Name, Date: i.Date, UpdatedAt: time.Now()}).Error
	if err != nil {
		return &Item{}, err
	}
	if i.ID != 0 {
		err := db.Debug().Model(&Dashboard{}).Where("id = ?", i.DashboardID).Take(&i.Dashboard).Error
		if err != nil {
			return &Item{}, err
		}
	}
	return i, nil
}

func (i *Item) DeleteItem(db *gorm.DB, did int, uid int) (int64, error) {
	err := db.Debug().Model(&Item{}).Where("id = ? and user_id = ?", did, uid).Take(&Item{}).Delete(&Item{}).Error
	if err != nil {
		if gorm.IsRecordNotFoundError(err) {
			return 0, errors.New("Item not Found")
		}
		return 0, err
	}
	return db.RowsAffected, nil
}
