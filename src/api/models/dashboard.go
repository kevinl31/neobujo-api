package models

import (
	"errors"
	"html"
	"strings"
	"time"

	"github.com/jinzhu/gorm"
)

// Dashboard entity containing items
type Dashboard struct {
	ID        int       `gorm:"primary_key" json:"id"`
	UserID    int       `gorm:"not null" json:"user_id"`
	Name      string    `gorm:"type:varchar(100)" json:"name"`
	Type      string    `gorm:"type:varchar(20)" json:"type"`
	CreatedAt time.Time `gorm:"default:CURRENT_TIMESTAMP" json:"created_at"`
	UpdatedAt time.Time `gorm:"default:CURRENT_TIMESTAMP" json:"updated_at"`
	User      User      `json:"user"`
	Items     []Item    `json:"items"`
}

func isInvalidDashboardType(dashboardType string) bool {
	switch dashboardType {
	case
		"six_month",
		"monthly",
		"weekly",
		"collection":
		return false
	}
	return true
}

func (d *Dashboard) Prepare() {
	d.Name = html.EscapeString(strings.TrimSpace(d.Name))
	d.CreatedAt = time.Now()
	d.UpdatedAt = time.Now()
}

func (d *Dashboard) Validate() error {
	if d.Name == "" {
		return errors.New("Required Name")
	}
	if d.UserID < 1 {
		return errors.New("Invalid User Id")
	}
	if isInvalidDashboardType(d.Type) {
		return errors.New("Invalid dashboard type")
	}
	return nil
}

func (d *Dashboard) SaveDashboard(db *gorm.DB) (*Dashboard, error) {
	err := db.Debug().Model(&Dashboard{}).Create(&d).Error
	if err != nil {
		return &Dashboard{}, err
	}
	if d.ID != 0 {
		// Fetching user related to the dashboard
		err = db.Debug().Model(&User{}).Where("id = ?", d.UserID).Take(&d.User).Error
		if err != nil {
			return &Dashboard{}, err
		}
	}
	return d, nil
}

func (d *Dashboard) FindAllDashboards(db *gorm.DB) (*[]Dashboard, error) {
	dashboards := []Dashboard{}
	err := db.Debug().Model(&Dashboard{}).Limit(100).Find(&dashboards).Error
	if err != nil {
		return &[]Dashboard{}, err
	}
	// Wondering if it's the best way to fetch User ...
	if len(dashboards) > 0 {
		for i, _ := range dashboards {
			err := db.Debug().Model(&User{}).Where("id = ?", dashboards[i].UserID).Take(&dashboards[i].User).Error
			if err != nil {
				return &[]Dashboard{}, err
			}
		}
	}
	return &dashboards, nil
}

func (d *Dashboard) FindDashboardByID(db *gorm.DB, did int) (*Dashboard, error) {
	err := db.Debug().Model(&Dashboard{}).Where("id = ?", did).Take(&d).Error
	if err != nil {
		return &Dashboard{}, err
	}
	if d.ID != 0 {
		err := db.Debug().Model(&User{}).Where("id = ?", d.UserID).Take(&d.User).Error
		if err != nil {
			return &Dashboard{}, err
		}
	}
	return d, nil
}

func (d *Dashboard) UpdateDashboard(db *gorm.DB) (*Dashboard, error) {
	err := db.Debug().Model(&Dashboard{}).Where("id = ?", d.ID).Updates(
		Dashboard{Name: d.Name, UpdatedAt: time.Now()},
	).Error
	if err != nil {
		return &Dashboard{}, err
	}
	if d.ID != 0 {
		err := db.Debug().Model(&User{}).Where("id = ?", d.UserID).Take(&d.User).Error
		if err != nil {
			return &Dashboard{}, err
		}
	}
	return d, nil
}

func (d *Dashboard) DeleteDashboard(db *gorm.DB, did int, uid int) (int64, error) {
	err := db.Debug().Model(&Dashboard{}).Where("id = ? and user_id = ?", did, uid).Take(&Dashboard{}).Delete(&Dashboard{}).Error
	if err != nil {
		if gorm.IsRecordNotFoundError(err) {
			return 0, errors.New("Dashboard not Found")
		}
		return 0, err
	}
	return db.RowsAffected, nil
}
